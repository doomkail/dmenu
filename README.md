# Doomkail's dmenu

Agregando algunas cosas al dmenu por defecto:

- Alpha patch, este es importante para implementar transparencia en st
- se pueden ver caracteres de colores como emojis (la libreria libxft-bgra es necesaria para esto)
- Puede leer Xresources
- Agregado un modo de Contraseñas ('-P')


## Instalacion

Es necesario tener `libxft-bgra` hasta que la version the libxft este corregida para ver colores.

Despues de cualquier cambio, se instala con `sudo make install`

